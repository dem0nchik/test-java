package com.example.demo;

import org.junit.Test;

import static org.junit.Assert.*;

public class SomeTest {
    @Test
    public void whenPalindrom_thenAccept() {
        Some palindromeTester = new Some();
        assertTrue(palindromeTester.isPalindrome("noon"));
    }

    @Test
    public void whenNearPalindrom_thanReject(){
        Some palindromeTester = new Some();
        assertFalse(palindromeTester.isPalindrome("neon"));
    }

    @Test
    public void testSum() {
        Some some = new Some();
        assertEquals(10, some.sum(5,5));
    }
}
